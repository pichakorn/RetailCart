
var AWS = require('aws-sdk');
// Set the region 
AWS.config.update({
    region: 'ap-southeast-1',
    endpoint: "http://localhost:8000",
});

const DynamoDB = require('aws-sdk/clients/dynamodb')

// Create DynamoDB document client
const DocumentClient = new DynamoDB.DocumentClient();


const { Table } = require('dynamodb-toolbox');

// Instantiate a table

const MyTable = new Table({
    // Specify table name (used by DynamoDB)
    name: 'RetailCart',
    // Define partition and sort keys
    partitionKey: 'PK',
    sortKey: 'SK',
    indexes: {
        "GSI1": { partitionKey: 'ItemSKU', sortKey: 'SK' }
    },

    // Add the DocumentClient
    DocumentClient
})

module.exports = MyTable;