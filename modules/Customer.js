var MyTable = require('./dynamodb');
const { Entity } = require('dynamodb-toolbox');


const Cart = new Entity({
    // Specify entity name
    name: 'Cart',
    // Define attributes
    attributes: {
        PK: { type: 'string', partitionKey: true }, // flag as partitionKey
        SK: { type: 'string', sortKey: true, hidden: true }, // flag as sortKey and mark hidden,
        Status: ['SK', 0],
        CreateTimestamp: ['SK', 1],
        ItemSKU: { type: 'string', prefix: 'SKU-' },
        OrderID: { type: 'string', required: false, prefix: 'Order_' }
    },
    timestamps: false,
    // Assign it to our table
    table: MyTable
})


module.exports.addItemtoCart = async function (data) {
    return new Promise(async function (resolve, reject) {
        try {
            let item = {
                PK: data.customerID,
                Status: 'ACTIVE',
                CreateTimestamp: new Date().toISOString(),
                ItemSKU: data.ItemSKU
            }

            // Use the 'put' method of Customer
            let result = await Cart.put(item);


            resolve({ status: true, msg: 'เพิิ่มสินค้าเข้าตะกร้าสำเร็จ' });
        }
        catch (e) {
            resolve({ status: false, msg: 'เกิดข้อผิดพลาด (DB)' });
        }
    })
};

module.exports.savedItem = async function (userID, itemSKU) {
    return new Promise(async function (resolve, reject) {
        try {

            let find_duplicate_save_item = await Cart.query(userID, { beginsWith: 'SAVED#', filters: { attr: 'ItemSKU', eq: `SKU-${itemSKU}` } });

            if (find_duplicate_save_item.Count < 1) {
                let item = {
                    PK: userID,
                    Status: 'SAVED',
                    CreateTimestamp: new Date().toISOString(),
                    ItemSKU: itemSKU
                }
                let result = await Cart.put(item);
                resolve({ status: true, msg: 'บันทึกสินค้าสำเร็จ' });
            }
            else {
                await Cart.delete(find_duplicate_save_item.Items[0])
                resolve({ status: false, msg: 'ลบการบันทึกสินค้านี้แล้ว' })
            }
        }
        catch (e) {
            console.log(e);
            resolve({ status: false, msg: 'เกิดข้อผิดพลาด (DB)' });
        }
    })
}



module.exports.checkOut = async function (userID) {
    return new Promise(async function (resolve, reject) {
        try {
            let my_cart = await Cart.query(userID, {
                beginsWith: 'ACTIVE#'
            });


            if (my_cart.Count < 1) {
                resolve({ status: false, msg: 'ไม่มีสินค้าในตะกร้า' });
                return;
            }

            let date = new Date().toISOString();
            let order_id = makeid(8);
            for (let i = 0; i < my_cart.Items.length; i++) {
                let item = JSON.parse(JSON.stringify(my_cart.Items[i]))
                let result = await Cart.delete({
                    PK: userID,
                    SK: `ACTIVE#${item.CreateTimestamp}`
                })

                item.Status = "PURCHASED";
                item.CreateTimestamp = date;
                item.OrderID = order_id;
                await Cart.update(item);
            }

            resolve({ status: true, msg: 'สั่งซื้อสินค้าสำเร็จ' });
        }
        catch (e) {
            console.log(e);
            resolve({ status: false, msg: 'เกิดข้อผิดพลาด (DB)' });
        }
    })
}


module.exports.viewHistoryByUserIdAndType = async function (userID, type) {
    return new Promise(async function (resolve, reject) {
        try {
            let find_duplicate_save_item = await Cart.query(userID, { beginsWith: `${type}#`, reverse: true });
            resolve(find_duplicate_save_item.Items);
        }
        catch (e) {
            resolve({ status: false, msg: 'เกิดข้อผิดพลาด' });
        }
    })
}


module.exports.getWhoselectItemSKU = async function (ItemSKU, TYPE) {
    return new Promise(async function (resolve, reject) {
        try {
            let arr_user = await Cart.query(`SKU-${ItemSKU}`, {
                index: 'GSI1',
                beginsWith: `${TYPE}#`,
                reverse: true
            });

            resolve(arr_user.Items);

        }
        catch (e) {
            resolve({ status: false, msg: 'เกิดข้อผิดพลาด (DB)' });
        }
    })
}


function makeid(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() *
            charactersLength));
    }
    return result;
}









