var express = require('express');
var router = express.Router();

var Customer = require('../modules/Customer');

/* GET home page. */



router.post('/user/addtocart', async function (req, res) {
    let result = await Customer.addItemtoCart(req.body);
    res.json(result);;
});


router.post('/user/checkout', async function (req, res) {
    let result = await Customer.checkOut(req.body.customerID);
    res.json(result);;
});


router.post('/user/savedItem', async function (req, res) {
    let result = await Customer.savedItem(req.body.customerID, req.body.ItemSKU);
    res.json(result);;
});


router.post('/user/history/purchase', async function (req, res) {
    let result = await Customer.viewHistoryByUserIdAndType(req.body.customerID, 'PURCHASED')
    res.json(result);
})

router.post('/user/history/save', async function (req, res) {
    let result = await Customer.viewHistoryByUserIdAndType(req.body.customerID, 'SAVED')
    res.json(result);
})

router.post('/user/history/active', async function (req, res) {
    let result = await Customer.viewHistoryByUserIdAndType(req.body.customerID, 'ACTIVE')
    res.json(result);
})

router.post('/findwhoselectthisitem', async function (req, res) {
    let result = await Customer.getWhoselectItemSKU(req.body.ItemSKU, req.body.type);
    res.json(result);
})

module.exports = router;
